//
//  LoginViewModelTests.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 17/11/2022.
//

import XCTest
import Combine
@testable import TwitterClientUIKit

class LoginViewModelTests: XCTestCase {
    
    private var loginViewModel: LoginViewModel!
    private var apiMock: APIManagerMock!
    private var coordinatorMock: CoordinatorMock!

    @MainActor func test_login() {
        givenAViewModel()
        apiMock.createUserResponse = Just(User(userName: "my name", fullName: "@myusername"))
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        loginViewModel.didTapLoginButton(username: "@username", fullname: "full name")
        
        XCTAssertTrue(coordinatorMock.showHomeModuleCalled)
    }
    
    @MainActor func test_loginFails() {
        givenAViewModel()

        apiMock.createUserResponse = Fail(error: MockError() as Error)
            .eraseToAnyPublisher()
        
        loginViewModel.didTapLoginButton(username: "@username", fullname: "full name")
        
//        XCTAssertFalse(coordinatorMock.showHomeModuleCalled)
    }
    
    @MainActor private func givenAViewModel() {
        apiMock = APIManagerMock()
        coordinatorMock = CoordinatorMock()
        loginViewModel = LoginViewModel(apiManager: apiMock, coordinator: coordinatorMock)
    }
}
