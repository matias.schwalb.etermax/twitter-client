//
//  APIManagerMock.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 17/11/2022.
//

@testable import TwitterClientUIKit
import Combine

class APIManagerMock: APIManager {

    var createUserResponse: AnyPublisher<User, Error>!
    var followUserResponse: AnyPublisher<(), Error>!
    var getFollowingResponse: AnyPublisher<[User], Error>!
    var getTweetsResponse: AnyPublisher<[Tweet], Error>!
    var createTweetResponse: AnyPublisher<Tweet, Error>!
    
    func createUser(username: String, fullname: String) -> AnyPublisher<User, Error> {
        return createUserResponse
    }
    
    func getFollowing(username: String) -> AnyPublisher<[User], Error> {
        return getFollowingResponse
    }
    
    func followUser(follower: String, followee: String) -> AnyPublisher<(), Error> {
        return followUserResponse
    }
    
    func getTweets(username: String) -> AnyPublisher<[Tweet], Error> {
        return getTweetsResponse
    }
    
    func createTweet(username: String, text: String) -> AnyPublisher<Tweet, Error> {
        return createTweetResponse
    }
}

