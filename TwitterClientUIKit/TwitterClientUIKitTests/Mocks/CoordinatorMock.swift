//
//  CoordinatorMock.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 17/11/2022.
//

@testable import TwitterClientUIKit

class CoordinatorMock: AppCoordinator {
    
    private(set) var showHomeModuleCalled = false
    
    func start() {}
    func showLoginModule() {}
    
    func showHomeModule() {
        showHomeModuleCalled = true
    }
    
    func showTweetsModule() {}
}
