//
//  AppCoordinatorTests.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 18/11/2022.
//

import XCTest
@testable import TwitterClientUIKit
import SwiftUI

class AppCoordinatorTests: XCTestCase {

    private var appCoordinator: AppCoordinator!
    private var navigationController: UINavigationController!
    
    private let timeout = 0.5
    
    func test_startShowsLoginModule() {
        // given
        givenAnAppCoordinator()
        
        //when
        appCoordinator.start()
        _ = XCTWaiter.wait(for: [expectation(description: "Wait for n seconds")], timeout: timeout)

        // then
        XCTAssertTrue(navigationController.viewControllers.first is UIHostingController<LoginViewController>)
    }
    
    func test_showLoginModuleShowsLoginModule() {
        // given
        givenAnAppCoordinator()
        
        //when
        appCoordinator.showLoginModule()
        _ = XCTWaiter.wait(for: [expectation(description: "Wait for n seconds")], timeout: timeout)

        // then
        XCTAssertTrue(navigationController.viewControllers.first is UIHostingController<LoginViewController>)
    }
    
    func test_showHomeModuleShowsHomeModule() {
        // given
        givenAnAppCoordinator()
        
        //when
        appCoordinator.showHomeModule()
        _ = XCTWaiter.wait(for: [expectation(description: "Wait for n seconds")], timeout: timeout)

        // then
        XCTAssertTrue(navigationController.viewControllers.first is UIHostingController<HomeViewController>)
    }
    
    func test_showTweetsModuleShowsTweetsModule() {
        // given
        givenAnAppCoordinator()
        
        //when
        appCoordinator.showTweetsModule()
        _ = XCTWaiter.wait(for: [expectation(description: "Wait for n seconds")], timeout: timeout)

        // then
        XCTAssertTrue(navigationController.viewControllers.first is UIHostingController<TweetsViewController>)
    }
    
    private func givenAnAppCoordinator() {
        navigationController = UINavigationController()
        appCoordinator = DefaultAppCoordinator(navigationController: navigationController)
    }
}
