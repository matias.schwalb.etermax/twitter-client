//
//  HomeViewModelTests.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 17/11/2022.
//

import XCTest
import Combine
@testable import TwitterClientUIKit

class HomeViewModelTests: XCTestCase {

    private var homeViewModel: HomeViewModel!
    private var apiManagerMock: APIManagerMock!
    private var coordinatorMock: CoordinatorMock!
    private var cancellable = Set<AnyCancellable>()
    
    func test_getFollowing() throws {
        givenHomeViewModel()
        givenALoggedUser()
        apiManagerMock.getFollowingResponse = Just([User(userName: "@user", fullName: "name")])
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        let expectation = expectation(description: "following")
        homeViewModel.$following
            .dropFirst()
            .sink {
                XCTAssertEqual($0, [TableUser(from: User(userName: "@user", fullName: "name"))])
                expectation.fulfill()
            }
            .store(in: &cancellable)
        
        homeViewModel.getFollowing()
        wait(for: [expectation], timeout: 3)
    }
    
    func test_followUser() {
        givenHomeViewModel()
        givenALoggedUser()
        apiManagerMock.followUserResponse = Just(())
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        apiManagerMock.getFollowingResponse = Just([User(userName: "@user", fullName: "name")])
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        let expectation = expectation(description: "follow")
        homeViewModel.$didFollowUser
            .dropFirst()
            .sink {
                XCTAssertEqual($0, true)
                expectation.fulfill()
            }
            .store(in: &cancellable)
        
        homeViewModel.followUser(username: "@user")
        wait(for: [expectation], timeout: 3)
    }
    
    func test_followUserFails() {
        givenHomeViewModel()
        givenALoggedUser()
        apiManagerMock.followUserResponse = Fail(error: MockError() as Error)
            .eraseToAnyPublisher()

        let expectation = expectation(description: "follow")
        homeViewModel.$didFollowUser
            .dropFirst()
            .sink {
                XCTAssertEqual($0, false)
                expectation.fulfill()
            }
            .store(in: &cancellable)
        
        homeViewModel.followUser(username: "@user")
        wait(for: [expectation], timeout: 3)
    }
    
    func test_getFullNameWithUserLogged() {
        givenALoggedUser()
        givenHomeViewModel()
        
        XCTAssertEqual("logged user", homeViewModel.getUserFullName())
    }
    
    func test_getFullNameWithNoUserLogged() {
        UserSession.shared.loggedUser = nil

        givenHomeViewModel()
        
        XCTAssertEqual("No logged user", homeViewModel.getUserFullName())
    }
    
    private func givenHomeViewModel() {
        self.coordinatorMock = CoordinatorMock()
        self.apiManagerMock = APIManagerMock()
        homeViewModel = HomeViewModel(apiManager: apiManagerMock, coordinator: coordinatorMock)
    }
    
    private func givenALoggedUser() {
        UserSession.shared.loggedUser = User(userName: "@logged", fullName: "logged user")
    }
}

// Extension taken from https://www.swiftbysundell.com/articles/unit-testing-combine-based-swift-code/
extension XCTestCase {
    func awaitPublisher<T: Publisher>(
        _ publisher: T,
        timeout: TimeInterval = 10,
        file: StaticString = #file,
        line: UInt = #line
    ) throws -> T.Output {
        // This time, we use Swift's Result type to keep track
        // of the result of our Combine pipeline:
        var result: Result<T.Output, Error>?
        let expectation = self.expectation(description: "Awaiting publisher")

        let cancellable = publisher.sink(
            receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    result = .failure(error)
                case .finished:
                    break
                }

                expectation.fulfill()
            },
            receiveValue: { value in
                result = .success(value)
            }
        )

        // Just like before, we await the expectation that we
        // created at the top of our test, and once done, we
        // also cancel our cancellable to avoid getting any
        // unused variable warnings:
        waitForExpectations(timeout: timeout)
        cancellable.cancel()

        // Here we pass the original file and line number that
        // our utility was called at, to tell XCTest to report
        // any encountered errors at that original call site:
        let unwrappedResult = try XCTUnwrap(
            result,
            "Awaited publisher did not produce any output",
            file: file,
            line: line
        )

        return try unwrappedResult.get()
    }
}
