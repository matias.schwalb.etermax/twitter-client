//
//  AppCoordinator.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import Foundation
import UIKit
import SwiftUI

protocol AppCoordinator {
    func start()
    func showLoginModule()
    func showHomeModule()
    func showTweetsModule(username: String)
}

class DefaultAppCoordinator: AppCoordinator {
    private var navigationController: UINavigationController
    private let apiManager = DefaultAPIManager()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
//        showLoginModule()
        showTweetsModule(username: "@matu")
    }
    
    func showLoginModule() {
        Task {
            let module = await LoginBuilder.build(coordinator: self, apiManager: apiManager)
            await navigationController.pushViewController(UIHostingController(rootView: module), animated: true)
        }
    }
    
    func showHomeModule() {
        let module = HomeBuilder.build(coordinator: self, apiManager: apiManager)
        DispatchQueue.main.async {
            self.navigationController.pushViewController(UIHostingController(rootView: module), animated: true)
        }
    }
    
    func showTweetsModule(username: String) {
        let module = TweetsModuleBuilder.build(username: username, coordinator: self, apiManager: apiManager)
        DispatchQueue.main.async {
            self.navigationController.pushViewController(UIHostingController(rootView: module), animated: true)
        }
    }
}


