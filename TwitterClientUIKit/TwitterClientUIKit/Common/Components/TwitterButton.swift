//
//  TwitterButton.swift
//  twitter-client
//
//  Created by Matías Schwalb on 15/11/2022.
//

import SwiftUI

struct TwitterButton: View {
    
    var title: String
    var onTap: () -> Void

    var body: some View {
        Button(title) {
            onTap()
        }
        .tint(TwitterClientColors.blue)
        .controlSize(.regular)
        .buttonStyle(.borderedProminent)
        .font(.title2)
        .padding()
    }
}

struct TwitterButton_Previews: PreviewProvider {
    static var previews: some View {
        TwitterButton(title: "This is a button") {
            print("Tapped")
        }
    }
}
