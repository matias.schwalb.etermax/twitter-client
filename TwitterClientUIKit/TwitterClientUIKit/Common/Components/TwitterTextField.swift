//
//  TwitterTextField.swift
//  twitter-client
//
//  Created by Matías Schwalb on 15/11/2022.
//

import SwiftUI

struct TwitterTextField: View {
    
    var placeholder: String
    @State var textBinding: Binding<String>
    
    var body: some View {
        TextField(placeholder, text: textBinding)
            .padding()
            .background(TwitterClientColors.extraLightGray)
            .cornerRadius(5.0)
            .padding(.horizontal)
    }
}


struct TwitterTextField_Previews: PreviewProvider {
    
    static var previews: some View {
//        TwitterTextField(placeholder: "Placeholder", textBinding: )
        Text("Hi")
    }
}
