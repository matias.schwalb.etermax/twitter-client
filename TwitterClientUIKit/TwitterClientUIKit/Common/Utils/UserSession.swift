//
//  UserSession.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import Foundation

final class UserSession {
    
    static var shared = UserSession()
    
    var loggedUser: User?

}
