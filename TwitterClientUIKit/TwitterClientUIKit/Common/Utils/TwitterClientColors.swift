//
//  TwitterClientColors.swift
//  twitter-client
//
//  Created by Matías Schwalb on 14/11/2022.
//

import SwiftUI

class TwitterClientColors {
    static var blue: Color = Color(.sRGB, red: 29/255, green: 161/255, blue: 242/255, opacity: 1)
    static var lightGray: Color = Color(red: 170/255, green: 184/255, blue: 194/255)
    static var extraLightGray: Color = Color(.sRGB, red: 225/255, green: 232/255, blue: 237/255, opacity: 1)
    static var extraExtraLightGray: Color = Color(.sRGB, red: 245/255, green: 248/255, blue: 250/255, opacity: 1)
}
