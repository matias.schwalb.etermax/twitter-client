//
//  FollowRequest.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import Foundation

struct FollowRequest: Encodable {
    var follower: String
    var followee: String
}
