//
//  Tweet.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 23/11/2022.
//

import Foundation

struct TweetDTO: Codable {
    var text: String
    var username: String
}

struct Tweet: Codable, Identifiable {
    var id = UUID()
    var text: String
    var user: String
    
    static func fromDTO(dto: TweetDTO) -> Tweet {
        return Tweet(text: dto.text, user: dto.username)
    }
}

extension Tweet: Equatable {
    static func ==(lhs: Tweet, rhs: Tweet) -> Bool {
        return (lhs.text == rhs.text) && (lhs.user == rhs.user)
    }
}
