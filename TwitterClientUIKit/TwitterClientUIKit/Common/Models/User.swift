//
//  User.swift
//  twitter-client
//
//  Created by Matías Schwalb on 15/11/2022.
//

struct User: Decodable, Equatable {
    let userName: String
    let fullName: String
    
    func toString() -> String {
        return "\(userName) - \(fullName)"
    }
}

struct UserRequest: Encodable {
    let userName: String
    let fullName: String
    
    enum CodingKeys: String, CodingKey {
        case userName = "user_name"
        case fullName = "full_name"
    }
}
