//
//  TableUser.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import Foundation

struct TableUser: Identifiable {
    var id = UUID()
    let userName: String
    let fullName: String
    
    init(from user: User) {
        userName = user.userName
        fullName = user.fullName
    }
}

extension TableUser: Equatable {
    static func ==(lhs: TableUser, rhs: TableUser) -> Bool {
        return (lhs.fullName == rhs.fullName) && (lhs.userName == rhs.userName)
    }
}
