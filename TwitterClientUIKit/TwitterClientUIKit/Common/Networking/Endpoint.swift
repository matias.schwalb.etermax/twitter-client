//
//  Endpoint.swift
//  twitter-client
//
//  Created by Matías Schwalb on 15/11/2022.
//

import Foundation
import SwiftUI

struct Endpoint {
    var path: String
    var body: Data?
    
    var url: URL? {
        return URL(string: "http://localhost:8080/\(path)")
    }
    
    var headers: [String: Any] {
        return [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
    }
}

extension Endpoint {
    static func user(username: String, fullname: String) -> Self {
        return Endpoint(path: "user", body: try! JSONEncoder().encode(UserRequest(userName: username, fullName: fullname)))
    }
    
    static func follow(follower: String, followee: String) -> Self {
        return Endpoint(path: "follow", body: try! JSONEncoder().encode(FollowRequest(follower: follower, followee: followee)))
    }
    
    static func getFollowing(username: String) -> Self {
        return Endpoint(path: "following/\(username)")
    }
    
    static func getTwets(username: String) -> Self {
        return Endpoint(path: "getTweets/\(username)")
    }
    
    static func createTweet(username: String, text: String) -> Self {
        return Endpoint(path: "tweet", body: try! JSONEncoder().encode(TweetDTO(text: text, username: username)))
    }
}
