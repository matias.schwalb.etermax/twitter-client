//
//  APIManager.swift
//  twitter-client
//
//  Created by Matías Schwalb on 15/11/2022.
//

// This class was built following this tutorial: https://betterprogramming.pub/implement-a-networking-layer-using-combine-in-swift-5-8a83e3ac9bae

import Foundation
import Combine

protocol APIManager {
    func createUser(username: String, fullname: String) -> AnyPublisher<User, Error>
    func getFollowing(username: String) -> AnyPublisher<[User], Error>
    func followUser(follower: String, followee: String) -> AnyPublisher<(), Error>
    func getTweets(username: String) -> AnyPublisher<[TweetDTO], Error>
    func createTweet(username: String, text: String) -> AnyPublisher<TweetDTO, Error>
}

class DefaultAPIManager: APIManager {
    func createUser(username: String, fullname: String) -> AnyPublisher<User, Error> {
        let endpoint = Endpoint.user(username: username, fullname: fullname)
        
        return post(for: endpoint)
            .map(\.data)
            .decode(type: User.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func followUser(follower: String, followee: String) -> AnyPublisher<(), Error> {
        let endpoint = Endpoint.follow(follower: follower, followee: followee)
        
        return post(for: endpoint)
            .compactMap { $0.response as? HTTPURLResponse }
            .receive(on: DispatchQueue.main)
            .mapError { error in
                return URLError()
            }
            .eraseToAnyPublisher()
    }
    
    func getFollowing(username: String) -> AnyPublisher<[User], Error> {
        let endpoint = Endpoint.getFollowing(username: username)
        
        return get(for: endpoint)
            .map(\.data)
            .decode(type: [User].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func getTweets(username: String) -> AnyPublisher<[TweetDTO], Error> {
        let endpoint = Endpoint.getTwets(username: username)
        
        return get(for: endpoint)
            .map(\.data)
            .decode(type: [TweetDTO].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func createTweet(username: String, text: String) -> AnyPublisher<TweetDTO, Error> {
        let endpoint = Endpoint.createTweet(username: username, text: text)
        
        return post(for: endpoint)
            .map(\.data)
            .decode(type: TweetDTO.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    private func post(for endpoint: Endpoint) -> AnyPublisher<URLSession.DataTaskPublisher.Output, URLSession.DataTaskPublisher.Failure> {
        guard let url = endpoint.url else {
            print("Invalid URL")
            return Fail(error: URLSession.DataTaskPublisher.Failure(_nsError: URLError())).eraseToAnyPublisher()
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = "POST"
        urlRequest.httpBody = endpoint.body
        
        endpoint.headers.forEach { (key, value) in
            if let value = value as? String {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .eraseToAnyPublisher()
    }
    
    private func get(for endpoint: Endpoint) -> AnyPublisher<URLSession.DataTaskPublisher.Output, URLSession.DataTaskPublisher.Failure> {
        guard let url = endpoint.url else {
            print("Invalid URL")
            return Fail(error: URLSession.DataTaskPublisher.Failure(_nsError: URLError())).eraseToAnyPublisher()
        }
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = "GET"
        urlRequest.httpBody = endpoint.body
        
        endpoint.headers.forEach { (key, value) in
            if let value = value as? String {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        return URLSession.shared.dataTaskPublisher(for: urlRequest)
            .eraseToAnyPublisher()
    }
}

class URLError: NSError {}
