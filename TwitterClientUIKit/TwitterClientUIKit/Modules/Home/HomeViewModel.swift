//
//  HomeViewModel.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import Combine
import Foundation

class HomeViewModel: ObservableObject {

    private let apiManager: APIManager
    private let coordinator: AppCoordinator
    private var cancellable = Set<AnyCancellable>()
    
    @Published private(set) var following: [TableUser] = []
    @Published private(set) var didFollowUser: Bool?
    
    init(apiManager: APIManager, coordinator: AppCoordinator) {
        self.apiManager = apiManager
        self.coordinator = coordinator
    }
    
    func followUser(username: String) {
        guard let follower = UserSession.shared.loggedUser?.userName else {
            print("Failed to follow: logged user not found")
            return
        }
        
        apiManager.followUser(follower: follower, followee: username)
            .sink { [weak self] completion in
                switch completion {
                case .failure(_):
                    self?.didFollowUser = false
                case .finished:
                    self?.didFollowUser = true
                    self?.getFollowing()
                }
            } receiveValue: { [weak self] _ in
                print("followed user")
            }.store(in: &cancellable)
    }

    func getFollowing() {
        guard let username = UserSession.shared.loggedUser?.userName else {
            print("Error on get following: logged user not found")
            return
        }
        apiManager.getFollowing(username: username)
            .receive(on: DispatchQueue.main)
            .sink { completion in
            switch completion {
            case let .failure(error):
                print("Error on get following: \(error)")
            case .finished:
                break
            }
        } receiveValue: { users in
            self.following = users.map { user in
                return TableUser(from: user)
            }
        }.store(in: &cancellable)
    }
    
    func getUserFullName() -> String {
        return UserSession.shared.loggedUser?.fullName ?? "No logged user"
    }
    
    func myTweetsPressed() {
        guard let username = UserSession.shared.loggedUser?.userName else {
            return
        }
        
        coordinator.showTweetsModule(username: username)
    }
}
