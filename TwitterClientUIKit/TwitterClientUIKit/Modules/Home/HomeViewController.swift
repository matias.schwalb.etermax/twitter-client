//
//  HomeViewController.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import SwiftUI

struct HomeViewController: View {
    
    @ObservedObject private var viewModel: HomeViewModel

    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }
    
    @State private var usernameToFollow = ""
    
    var body: some View {
        VStack {
            Text(viewModel.getUserFullName())
                .font(.largeTitle)
            HStack {
                TwitterTextField(placeholder: "Username", textBinding: $usernameToFollow)
                TwitterButton(title: "Follow") {
                    viewModel.followUser(username: usernameToFollow)
                }
            }
            Section("Following") {
                List(viewModel.following) { user in
                    Text(user.userName)
                }
            }
            Section("Profile") {
                TwitterButton(title: "My Tweets") {
                    viewModel.myTweetsPressed()
                }
            }
        }.onAppear(perform: onAppear)
    }
    
    private func onAppear() {
        viewModel.getFollowing()
    }
}

struct HomeViewController_Previews: PreviewProvider {
    static var previews: some View {
        HomeViewController(viewModel: HomeViewModel(apiManager: DefaultAPIManager(), coordinator: DefaultAppCoordinator(navigationController: UINavigationController())))
    }
}
