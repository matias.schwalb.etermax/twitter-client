//
//  HomeBuilder.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

final class HomeBuilder {
    static func build(coordinator: AppCoordinator, apiManager: APIManager) -> HomeViewController {
        let viewModel = HomeViewModel(apiManager: apiManager, coordinator: coordinator)
        
        return HomeViewController(viewModel: viewModel)
    }
}
