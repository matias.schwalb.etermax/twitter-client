//
//  TweetsViewController.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 18/11/2022.
//

import SwiftUI

struct TweetsViewController: View {
    
    @ObservedObject private var viewModel: TweetsViewModel

//    @State private var showingAlert = false
    @State private var alertInput: String = ""
    
    init(viewModel: TweetsViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        ZStack() {

            VStack() {
                Text("My Tweets")
                    .font(.largeTitle)
                List(viewModel.tweets) { tweet in
                    VStack {
                        Text(tweet.user)
                            .font(.headline)
                        Text(tweet.text)
                            .font(.body)
                            .padding()
                    }
                }.listStyle(.plain)
                HStack {
                    Spacer()
                    Button(action: {
                        viewModel.shouldShowAlert = true
                    }) {
                        Image(systemName: "square.and.pencil")
                            .frame(width: 75, height: 75, alignment: .center)
                            .tint(.white)
                            .background(TwitterClientColors.blue)
                            .clipShape(Circle())
                    }.padding(.trailing)
                        .sheet(isPresented: $viewModel.shouldShowAlert) {
                            VStack() {
                                Text("Create new tweet")
                                    .font(.largeTitle)
                                TwitterTextField(placeholder: "Text", textBinding: $alertInput)
                                TwitterButton(title: "Tweet") {
                                    viewModel.createTweet(text: alertInput)
                                }
                            }
                            .background(TwitterClientColors.lightGray)
                            .cornerRadius(25)
                            .padding()
                        }
                }
            }.onAppear(perform: onAppear)

        }
    }
    
    private func onAppear() {
        viewModel.getTweets()
    }
}

struct TweetsViewController_Previews: PreviewProvider {
    static var previews: some View {
        TweetsViewController(viewModel: TweetsViewModel(username: "@matu", apiManager: DefaultAPIManager(), coordinator: DefaultAppCoordinator(navigationController: UINavigationController())))
    }
}

