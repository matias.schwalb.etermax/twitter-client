//
//  TweetsModuleBuilder.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 23/11/2022.
//

final class TweetsModuleBuilder {
    static func build(username: String, coordinator: AppCoordinator, apiManager: APIManager) -> TweetsViewController {
        let viewModel = TweetsViewModel(username: username, apiManager: apiManager, coordinator: coordinator)
        return TweetsViewController(viewModel: viewModel)
    }
}
