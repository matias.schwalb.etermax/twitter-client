//
//  TweetsViewModel.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 18/11/2022.
//

import Combine
import Foundation

final class TweetsViewModel: ObservableObject {
    
    private let apiManager: APIManager
    private let coordinator: AppCoordinator
    private var cancellable = Set<AnyCancellable>()
    private let username: String
    
    @Published private(set) var tweets: [Tweet] = []
    @Published private(set) var createdTweet: Tweet?
    @Published var shouldShowAlert = false
    
    init(username: String, apiManager: APIManager, coordinator: AppCoordinator) {
        self.username = username
        self.apiManager = apiManager
        self.coordinator = coordinator
    }
    
    func getTweets() {
        apiManager.getTweets(username: username)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    break
                case .finished:
                    break
                }
            } receiveValue: { [weak self] tweets in
                self?.tweets = tweets.map { tweetDTO in
                    return Tweet.fromDTO(dto: tweetDTO)
                }
            }.store(in: &cancellable)
    }
    
    func createTweet(text: String) {
        apiManager.createTweet(username: username, text: text)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(_):
                    self.shouldShowAlert = false
                case .finished:
                    self.shouldShowAlert = false
                    self.getTweets()
                }
            } receiveValue: { [weak self] tweetDTO in
                self?.createdTweet = Tweet.fromDTO(dto: tweetDTO)
            }.store(in: &cancellable)
    }
}
