//
//  LoginBuilder.swift
//  twitter-client
//
//  Created by Matías Schwalb on 16/11/2022.
//

class LoginBuilder {
    @MainActor static func build(coordinator: AppCoordinator, apiManager: APIManager) -> LoginViewController {
        let viewModel = LoginViewModel(apiManager: apiManager, coordinator: coordinator)
        return LoginViewController(viewModel: viewModel)
    }
}
