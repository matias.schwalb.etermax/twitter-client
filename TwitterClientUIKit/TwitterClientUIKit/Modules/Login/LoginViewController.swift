//
//  LoginViewController.swift
//  TwitterClientUIKit
//
//  Created by Matías Schwalb on 17/11/2022.
//

import SwiftUI

struct LoginViewController: View {
    
    @State var username: String = ""
    @State var fullName: String = ""
    
    @ObservedObject var viewModel: LoginViewModel
    
    var body: some View {
        VStack{
            twitterImage()
            TwitterTextField(placeholder: "Username", textBinding: $username)
            TwitterTextField(placeholder: "Full name", textBinding: $fullName)
            TwitterButton(title: "Login") {
                viewModel.didTapLoginButton(username: username, fullname: fullName)
            }
        }
    }
    
    private func twitterImage() -> some View {
        return Image("twitter")
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 150, height: 150, alignment: .center)
            .padding(.bottom, 25)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginViewController(viewModel: LoginViewModel(apiManager: DefaultAPIManager(), coordinator: DefaultAppCoordinator(navigationController: UINavigationController())))
        }
    }
}
