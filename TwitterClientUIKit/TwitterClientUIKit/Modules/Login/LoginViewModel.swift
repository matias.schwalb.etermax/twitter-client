//
//  LoginViewModel.swift
//  twitter-client
//
//  Created by Matías Schwalb on 14/11/2022.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    
    private let apiManager: APIManager
    private let coordinator: AppCoordinator
    private var cancellable = Set<AnyCancellable>()

    init(apiManager: APIManager, coordinator: AppCoordinator) {
        self.apiManager = apiManager
        self.coordinator = coordinator
    }
    
    private func executeLogin(username: String, fullname: String) {
        apiManager.createUser(username: username, fullname: fullname)
            .sink { completion in
                switch completion {
                case let .failure(error):
                    print("Error on creating user: \(error)")
                case .finished:
                    break
                }
            } receiveValue: { [weak self] user in
                UserSession.shared.loggedUser = user
                print(user.toString())
                self?.coordinator.showHomeModule()
            }.store(in: &cancellable)
    }
    
    func didTapLoginButton(username: String, fullname: String) {
//        executeLogin(username: username, fullname: fullname)
        UserSession.shared.loggedUser = User(userName: "@matu", fullName: "Matuli")
        coordinator.showHomeModule()
    }
}

