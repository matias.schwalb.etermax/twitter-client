//
//  TweetsViewModelTests.swift
//  TwitterClientUIKitTests
//
//  Created by Matías Schwalb on 23/11/2022.
//

import XCTest
import Combine
@testable import TwitterClientUIKit

class TweetsViewModelTests: XCTestCase {

    private var tweetsViewModel: TweetsViewModel!
    private var apiManagerMock: APIManagerMock!
    private var coordinatorMock: CoordinatorMock!
    private var cancellable = Set<AnyCancellable>()
    
    private let username = "@matu"
    private let tweet = Tweet(text: "hi!", user: "@matu")
    
    private let expectationTimeout = 3.0
    
    func test_getTweets() {
        givenTweetsViewModel()
        apiManagerMock.getTweetsResponse = Just([tweet])
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        let expectation = expectation(description: "get tweets")
        tweetsViewModel.$tweets
            .dropFirst()
            .sink {
                XCTAssertEqual($0, [Tweet(text: "hi!", user: "@matu")])
                expectation.fulfill()
            }
            .store(in: &cancellable)
        
        tweetsViewModel.getTweets()
        wait(for: [expectation], timeout: expectationTimeout)
    }
    
    func test_createTweet() {
        givenTweetsViewModel()
        apiManagerMock.createTweetResponse = Just(tweet)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
        
        let expectation = expectation(description: "create tweet")
        tweetsViewModel.$createdTweet
            .dropFirst()
            .sink {
                XCTAssertEqual($0, self.tweet)
                expectation.fulfill()
            }
            .store(in: &cancellable)
        
        tweetsViewModel.createTweet(text: "hi!")
        wait(for: [expectation], timeout: expectationTimeout)
    }
    
    private func givenTweetsViewModel() {
        apiManagerMock = APIManagerMock()
        coordinatorMock = CoordinatorMock()
        tweetsViewModel = TweetsViewModel(username: username, apiManager: apiManagerMock, coordinator: coordinatorMock)
    }
}
